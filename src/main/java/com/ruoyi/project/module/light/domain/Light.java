package com.ruoyi.project.module.light.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 亮点对象 tb_light
 * 
 * @author ruoyi
 * @date 2020-05-08
 */
public class Light extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long lightId;

    /** 亮点标题 */
    @Excel(name = "亮点标题")
    private String lightTitle;

    /** 亮点主图 */
    @Excel(name = "亮点主图")
    private String lightPic;

    /** 亮点内容 */
    @Excel(name = "亮点内容")
    private String lightContent;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setLightId(Long lightId) 
    {
        this.lightId = lightId;
    }

    public Long getLightId() 
    {
        return lightId;
    }
    public void setLightTitle(String lightTitle) 
    {
        this.lightTitle = lightTitle;
    }

    public String getLightTitle() 
    {
        return lightTitle;
    }
    public void setLightPic(String lightPic) 
    {
        this.lightPic = lightPic;
    }

    public String getLightPic() 
    {
        return lightPic;
    }
    public void setLightContent(String lightContent) 
    {
        this.lightContent = lightContent;
    }

    public String getLightContent() 
    {
        return lightContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("lightId", getLightId())
            .append("lightTitle", getLightTitle())
            .append("lightPic", getLightPic())
            .append("lightContent", getLightContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
