package com.ruoyi.project.module.tale.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 小组故事对象 tb_tale
 * 
 * @author ruoyi
 * @date 2020-05-09
 */
public class Tale extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long taleId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    private String deptName;

    /** 提交人id */
    @Excel(name = "提交人id")
    private Long userId;

    /** 提交人姓名 */
    @Excel(name = "提交人姓名")
    private String userName;

    /** 提交时间 */
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date taleTime;

    /** 故事标题 */
    @Excel(name = "故事标题")
    private String taleTitle;

    /** 故事内容 */
    @Excel(name = "故事内容")
    private String taleContent;

    /** 故事图片 */
    @Excel(name = "故事图片")
    private String taleImg;

    /** 故事视频 */
    @Excel(name = "故事视频")
    private String taleVideo;

    /** 状态（0正常 1关闭） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    public void setTaleId(Long taleId) 
    {
        this.taleId = taleId;
    }

    public Long getTaleId() 
    {
        return taleId;
    }
    public void setDeptId(Long deptId) 
    {
        this.deptId = deptId;
    }

    public Long getDeptId() 
    {
        return deptId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setTaleTime(Date taleTime) 
    {
        this.taleTime = taleTime;
    }

    public Date getTaleTime() 
    {
        return taleTime;
    }
    public void setTaleTitle(String taleTitle) 
    {
        this.taleTitle = taleTitle;
    }

    public String getTaleTitle() 
    {
        return taleTitle;
    }
    public void setTaleContent(String taleContent) 
    {
        this.taleContent = taleContent;
    }

    public String getTaleContent() 
    {
        return taleContent;
    }
    public void setTaleImg(String taleImg) 
    {
        this.taleImg = taleImg;
    }

    public String getTaleImg() 
    {
        return taleImg;
    }
    public void setTaleVideo(String taleVideo) 
    {
        this.taleVideo = taleVideo;
    }

    public String getTaleVideo() 
    {
        return taleVideo;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("taleId", getTaleId())
            .append("deptId", getDeptId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("taleTime", getTaleTime())
            .append("taleTitle", getTaleTitle())
            .append("taleContent", getTaleContent())
            .append("taleImg", getTaleImg())
            .append("taleVideo", getTaleVideo())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
